README to come....

Based heavily on the Heroku example java project: https://devcenter.heroku.com/articles/getting-started-with-java

Massive hat tip to Taylor Brennan's Slackbot/Heroku guide: http://www.sitepoint.com/getting-started-slack-bots/

Further shout-out to Robert Schultz's Hearthstone JSON project: http://hearthstonejson.com/

The gist is to deploy this guy on Heroku, point a Slack slash command at it, and it'll work.

edit hearthbot.properties to include your Slack incoming webhook URL, a personal key for accessing the omgvamp Hearthstone API on Mashape: https://www.mashape.com/omgvamp/hearthstone, and (if you want) the name/icon for your bot

I can be reached on Twitter @caryy if you have any questions.