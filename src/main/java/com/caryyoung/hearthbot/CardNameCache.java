package com.caryyoung.hearthbot;




import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;


/**
 * Contains a set with all of the Hearthstone card names, as well as a cache of previously-searched results.
 */
public class CardNameCache {

    private Set<String> cardNames;
    private Map<String, CardSuggestor.Suggestion> previousSearches;
    public static int CACHE_MAX = 100000;
    private static CardNameCache cardNameCache;
    private CardNameCache() {
        InputStream stream = CardNameCache.class.getResourceAsStream("/com/caryyoung/hearthbot/cards.txt");
        String cards = "";
        try {
            cards = IOUtils.toString(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] cardsArray = cards.split("\n");
        List<String> cardsList = Lists.newArrayList(cardsArray);
        cardNames = new HashSet<String>(cardsList);
        previousSearches = new HashMap<String, CardSuggestor.Suggestion>();
    }

    public Set<String> getCardNames() {
        if (previousSearches.size() > CACHE_MAX) {
           previousSearches.clear();
        }
        return cardNames;
    }

    public void addToCardNameSet(String cardName) {
        cardNames.add(cardName);
    }

    public void removeFromCardNameSet(String cardName) {
        cardNames.remove(cardName);
    }

    public void addSearchResults(String searchTerm, CardSuggestor.Suggestion suggestions) {
       previousSearches.put(searchTerm, suggestions);
    }

    public CardSuggestor.Suggestion getSearchResults(String searchTerm) {
        if (previousSearches.containsKey(searchTerm)) {
            return previousSearches.get(searchTerm);
        } return null;
    }

    public static CardNameCache getInstance() {
        if( cardNameCache == null) {
            cardNameCache = new CardNameCache();
        }
        return cardNameCache;
    }
}
