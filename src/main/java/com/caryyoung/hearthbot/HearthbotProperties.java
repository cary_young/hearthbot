package com.caryyoung.hearthbot;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Properties for deploy-specific information like URLs and credentials.
 */
public class HearthbotProperties {
    private static Properties properties;
    public static String getProperty(String property) {
        if (properties == null) {
            InputStream propertiesFileStream = HearthbotProperties.class.getResourceAsStream("/com/caryyoung/hearthbot/hearthbot.properties");
            properties = new Properties();
            try {
                properties.load(propertiesFileStream);
            } catch (IOException e) {
                e.printStackTrace(); //TODO something more robust
            }
        }
        return properties.getProperty(property);
    }
}

