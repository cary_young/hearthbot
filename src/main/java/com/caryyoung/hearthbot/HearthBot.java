package com.caryyoung.hearthbot;

import com.google.common.net.UrlEscapers;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class HearthBot {

    /**
     * Entry point for Hearthbot.  Give it a card name and it'll return a URL to an image of that card.
     * @param cardName
     * @return url to card image, or an error message if something went wrong.
     */
    public static String getCardImage(String cardName) {
        try {
            HearthstoneCard card = getCard(cardName);
            return card.imageUrl;
        } catch (HearthBotException ex) {
            return "Error: " + ex.getMessage();
        }
    }


    public static String getFullCardInformation(String cardName) {
        try {
            HearthstoneCard card = getCard(cardName);
            return card.toSlackFormattedStatsString();
        } catch (HearthBotException ex) {
            return "Error: " + ex.getMessage();
        }
    }

    /**
     * Make a request to the hearthstone card API at Mashape for the specified card
     * @param cardName
     * @return
     * @throws HearthBotException
     */
    private static JSONObject makeCardRequest(String cardName) throws HearthBotException {
        HttpResponse<JsonNode> response = null;
        try {
            final String cardNameEncoded = UrlEscapers.urlPathSegmentEscaper().escape(cardName);
            response = Unirest.get("https://omgvamp-hearthstone-v1.p.mashape.com/cards/" + cardNameEncoded)
                    .header("X-Mashape-Key", HearthbotProperties.getProperty("mashape.key"))
                    .asJson();

        } catch (UnirestException ex) {
            throw new HearthBotException("Error connecting to card API :(");
        }

        if (response == null) {
            throw new HearthBotException("Error connecting to card API :(");
        }

        JsonNode respJson = response.getBody();
        JSONObject cardInfo = null;
        JSONArray responseArray = respJson.getArray();

        try {
            cardInfo = responseArray.getJSONObject(0);
            if (responseArray.length() > 1) {
                // the cards that take over your hero portrait (e.g. Jaraxxus) have Hearthstone card objects in the game,
                // but aren't the same as the playable card.  Here we select for the card-version and not the portrait.
                if ( "hero".equalsIgnoreCase(cardInfo.getString("type")) ) {
                    cardInfo = responseArray.getJSONObject(1);
                } else {
                    for (int i = 0; i < responseArray.length(); i++) {
                        cardInfo = responseArray.getJSONObject(i);
                        // in the explorers league expansion I noticed the unearthed raptor card has a weird half-null
                        // entry in addition to the expected entry for the card.  this is to select for the good entry.
                        if (cardInfo.has("img")) {
                            break;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            throw new HearthBotException("Error processing response from card API :(");
        }
        return cardInfo;
    }

    public static HearthstoneCard getCard(String cardName) throws HearthBotException {
        JSONObject response = null;
        response = makeCardRequest(cardName);
        final HearthstoneCard card = fromResponseJson(response);
        return card;
    }

    /**
     * Parse the response from the Hearthstone card API into a POJO
     * @param response
     * @return
     */
    private static HearthstoneCard fromResponseJson(JSONObject response) {

        // F this
        HearthstoneCard card = new HearthstoneCard();
        try {
            card.setArtist(response.getString("artist"));
        } catch (JSONException e) {
        }
        try {
            card.setCardId(response.getString("cardId"));
        } catch (JSONException e) {
        }
        try {
            card.setName(response.getString("name"));
        } catch (JSONException e) {

        }
        try {
            card.setSet(response.getString("cardSet"));
        } catch (JSONException e) {
        }
        try {
            card.setType(response.getString("type"));
        } catch (JSONException e) {
        }
        try {
            card.setFaction(response.getString("faction"));
        } catch (JSONException e) {
        }
        try {
            card.setRarity(response.getString("rarity"));
        } catch (JSONException e) {
        }
        try {
            card.setManaCost(response.getInt("cost"));
        } catch (JSONException e) {
        }
        try {
            card.setAttack(response.getInt("attack"));
        } catch (JSONException e) {
        }
        try {
            card.setHealth(response.getInt("health"));
        } catch (JSONException e) {
        }
        try {
            card.setText(response.getString("text"));
        } catch (JSONException e) {
        }
        try {
            card.setFlavor(response.getString("flavor"));
        } catch (JSONException e) {
        }
        try {
            card.setCollectible(response.getBoolean("collectible"));
        } catch (JSONException e) {
        }
        try {
            card.setElite(response.getBoolean("elite"));
        } catch (JSONException e) {
        }
        try {
            card.setLocale(response.getString("locale"));
        } catch (JSONException e) {
        }
        try {
            card.setImageUrl(response.getString("img"));
        } catch (JSONException e) {
        }
        try {
            card.setGoldImageUrl(response.getString("goldImg"));
        } catch (JSONException e) {
        }

        return card;
    }

    /**
     * testing
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        System.out.println(HearthBot.getFullCardInformation("unearthed raptor"));
    }
}
