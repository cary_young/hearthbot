package com.caryyoung.hearthbot;
/**
 * I like making my own exceptions
 */
public class HearthBotException extends Exception {
    public HearthBotException(String msg) {
        super(msg);

    }
}
