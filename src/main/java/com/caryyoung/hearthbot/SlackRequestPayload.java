package com.caryyoung.hearthbot;

public class SlackRequestPayload {
    String text;
    String username = HearthbotProperties.getProperty("hearthbot.name");
    String icon_url = HearthbotProperties.getProperty("hearthbot.icon");

    /**
     * Using this constructor will prepend the string "[username] requested:\n" to the rest of the text.
     *
     * If you don't want that, use the text-only constructor or pass {@code null} for username.
     * @param username
     * @param text
     */
    public SlackRequestPayload(String username, String text) {
        if (username != null && !username.trim().isEmpty()) {
            text = username + " requested:\n" + text;
        }
        this.text = text;
    }

    public SlackRequestPayload(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public static void main(String[] args) {
        SlackRequestPayload p = new SlackRequestPayload("fdsfsd","sdfdsf");
        System.out.println(p.getIcon_url()
        );
        System.out.println(HearthbotProperties.getProperty("hearthbot.icon"));
    }
}

