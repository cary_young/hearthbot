package com.caryyoung.hearthbot;

/**
 * pojo for holding information about a Hearthstone card
 */
public class HearthstoneCard {
    public String cardId;
    public String name;
    public String set;
    public String type;
    public String faction;
    public Integer manaCost ;
    public String rarity;
    public Integer attack;
    public Integer health;
    public String text;
    public String flavor;
    public String artist;
    public boolean collectible;
    public boolean elite;
    public String imageUrl ;
    public String goldImageUrl;
    public String locale;

    public static final String REGEX_STRIP_HTML_TAGS = "\\<[^>]*>";

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public Integer getManaCost() {
        return manaCost;
    }

    public void setManaCost(Integer manaCost) {
        this.manaCost = manaCost;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getRarityColor() {
        if ("common".equalsIgnoreCase(rarity)) {
            return "white";
        } else if ("free".equalsIgnoreCase(rarity)) {
            return "no color";
        } else if ("rare".equalsIgnoreCase(rarity)) {
            return "blue";
        } else if ("epic".equalsIgnoreCase(rarity))  {
            return "purple";
        } else if ("legendary".equalsIgnoreCase(rarity)) {
            return "orange";
        }
        return "[color]";  // oops
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public Integer getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public boolean isCollectible() {
        return collectible;
    }

    public void setCollectible(boolean collectible) {
        this.collectible = collectible;
    }

    public boolean isElite() {
        return elite;
    }

    public void setElite(boolean elite) {
        this.elite = elite;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getGoldImageUrl() {
        return goldImageUrl;
    }

    public void setGoldImageUrl(String goldImageUrl) {
        this.goldImageUrl = goldImageUrl;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String toSlackFormattedStatsString() {
        StringBuilder cardStats = new StringBuilder();
        cardStats.append("*").append(name).append("*\n");
        cardStats.append("```");
        cardStats.append("Mana cost: ").append(manaCost).append("\n");
        cardStats.append("Attack: ").append(attack).append("\n");
        cardStats.append("Health: ").append(health).append("\n");
        cardStats.append("Card Text: ").append(text.replaceAll(REGEX_STRIP_HTML_TAGS,"")).append("\n");
        cardStats.append("Type: ").append(type).append("\n");
        cardStats.append("Set: ").append(set).append("\n");
        cardStats.append("Faction: ").append(faction).append("\n");
        cardStats.append("Rarity: ").append(rarity).append(" (").append(getRarityColor()).append(")\n");
        if (elite) {
            cardStats.append("Elite (Maximum one per deck)").append("\n");
        }
        if (collectible) {
            cardStats.append("Collectible").append("\n");
        }
        cardStats.append("Card ID: ").append(cardId).append("\n");
        cardStats.append("Artist: ").append(artist).append("\n");
        cardStats.append("```");
        return cardStats.toString();
    }
}
