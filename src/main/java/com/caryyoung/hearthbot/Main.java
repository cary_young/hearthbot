package com.caryyoung.hearthbot;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.*;


import java.util.Map;
import java.util.Set;

public class Main extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        showHome(req,resp);

    }

    /**
     * entry point for heroku endpoint
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    private void showHome(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        Map<String, String[]> requestMap = req.getParameterMap();
        try {
            String user = requestMap.get("user_name")[0];
            String slashArgument = requestMap.get("text")[0];
            String cardInfo;

            if (slashArgument != null && slashArgument.trim().startsWith("stats")) {
                slashArgument = slashArgument.replace("stats","").trim(); // remove the "stats" part of the string
                cardInfo = HearthBot.getFullCardInformation(slashArgument);
            } else {
                cardInfo = HearthBot.getCardImage(slashArgument);
            }

            if (cardInfo != null) {
                // post the card info to the channel as HearthBot
                postToSlack(user, cardInfo);
            } else {
                // figure out if they likely misspelled something
                CardSuggestor.Suggestion suggestions = CardSuggestor.getSuggestions(requestMap.get("text")[0]);

                // if there's only one suggested alternative, just pretend they asked for it.
                if (suggestions.getSuggestions().size() == 1) {
                    cardInfo = HearthBot.getCardImage(suggestions.getSuggestions().iterator().next());
                    if (cardInfo != null) {
                        postToSlack(user, cardInfo);
                        return;
                    }
                }

                // at this point, we can't figure out what they asked for, and there's more than one suggestion...
                // bail with an error message (containing applicable suggestions)

                // This will be shown only to the user by slackbot
                String suggestionMessage = suggestions.getSuggestionMessage(requestMap.get("text")[0]);
                String couldntFindCardMessage = "You asked for '" + requestMap.get("text")[0] + "', but I couldn't find it.  (Is it spelled correctly?)";
                if (suggestionMessage != null){
                resp.getWriter().print(suggestionMessage);
                } else {
                   resp.getWriter().print(couldntFindCardMessage);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);    // goes to logz i think
        }
    }

    /**
     * Post the response from Hearthbot to the Slack channel.
     * @param hearthBotResponse
     * @return
     */
    private void postToSlack(String username, String hearthBotResponse) {
        HttpClient httpClient = HttpClientBuilder.create().build(); //Use this instead

        String slackIncomingHookUrl = HearthbotProperties.getProperty("slack.incoming.webhook");
        Gson gson = new Gson();
        SlackRequestPayload slackPayload = new SlackRequestPayload(username, hearthBotResponse);
        HttpPost post = new HttpPost(slackIncomingHookUrl);
        try {
            StringEntity postingString = new StringEntity(gson.toJson(slackPayload));
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            HttpResponse response = httpClient.execute(post);

        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    public static void main(String[] args) throws Exception {
        Server server = new Server(Integer.valueOf(System.getenv("PORT")));
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        context.addServlet(new ServletHolder(new Main()),"/*");
        server.start();
        server.join();
    }
}
