package com.caryyoung.hearthbot;


import org.apache.commons.lang.StringUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Suggests possible cards when the user searches for a card that doesn't actually exist.
 */
public class CardSuggestor {

    public static Suggestion getSuggestions(String term) {
        if (CardNameCache.getInstance().getSearchResults(term) != null ) {
           return CardNameCache.getInstance().getSearchResults(term);
        }

        Set<String> cardNames = CardNameCache.getInstance().getCardNames();
        Set<String> suggestedCards = new HashSet<String>(5);
        for(String card : cardNames) {
            if (card.toLowerCase().startsWith(term)) {
                suggestedCards.add(card);
            } else if(term.length() > 5 && StringUtils.getLevenshteinDistance(card.toLowerCase(), term.toLowerCase()) <= 5) {
                suggestedCards.add(card);
            }
        }
        Suggestion suggestion = new Suggestion(term, suggestedCards);
        CardNameCache.getInstance().addSearchResults(term, suggestion);
        return suggestion;
    }

    public  static class Suggestion {
        Set<String> suggestions;
        public Suggestion(String searchTerm, Set<String> suggestions) {
            this.suggestions = suggestions;
        }

        public String getSuggestionMessage(String searchTerm) {
            if (suggestions.isEmpty()) {
                return null;
            }
            StringBuilder suggestionsString = new StringBuilder();
            for (String suggestion : suggestions) {
                suggestionsString.append("'").append(suggestion).append("', ");
            }
            suggestionsString.replace(suggestionsString.length()-2, suggestionsString.length(),"");
            StringBuilder message = new StringBuilder("You asked for '" + searchTerm + "' but I couldn't find it. Suggestions: " + suggestionsString.toString() );
            return message.toString();
        }

        public Set<String> getSuggestions() {
            return suggestions;
        }
    }
}

